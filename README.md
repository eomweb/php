[![build status](https://gitlab.com/e-om/php/badges/master/build.svg)](https://gitlab.com/e-om/php/commits/master) [![coverage report](https://gitlab.com/e-om/php/badges/master/coverage.svg)](https://gitlab.com/e-om/php/commits/master)

## Ejemplo de Test con PHP
Esta versión tiene un ejemplo de **CI** con **(PHP 5.6 + MySQL + PHPUnit + xDebug)** "Coverage Code" de como utilizar los pipelinos en GitLab y BitBucket,.
Los archivos de configuración son estos dos archivos.

**GitLab:** .gitlab-ci.yml

**BitBucket:** bitbucket-pipelines.yml

### Lo bueno de GitLab que podemos tener estos en los reportes ;)

[![build status](https://gitlab.com/e-om/php/badges/master/build.svg)](https://gitlab.com/e-om/php/commits/master)
[![coverage report](https://gitlab.com/e-om/php/badges/master/coverage.svg)](https://gitlab.com/e-om/php/commits/master)

Configurando esto dentro del proyecto tambien.
Print:
![image](/uploads/78fb71952734fcf1192699ef8e60d1c7/image.png)

